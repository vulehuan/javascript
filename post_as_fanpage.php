<script type="text/javascript">
    window.fbAsyncInit = function() {
        FB.init({
            appId: 'YOUR_APP_ID',
            channelUrl: '',
            status: true,
            cookie: true,
            xfbml: true
        });
        FB.getLoginStatus(function(response) {
            if (response.status == 'not_authorized') {
                FB.login(function(response) {
                    if (response.authResponse) {
                        //
                    } else {
                        //User cancelled login or did not fully authorize.
                    }
                }, {scope: 'manage_pages,publish_stream'});
            }
        });
    };
    //
    (function(d, debug) {
        var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement('script');
        js.id = id;
        js.async = true;
        js.src = "//connect.facebook.net/en_US/all" + (debug ? "/debug" : "") + ".js";
        ref.parentNode.insertBefore(js, ref);
    }(document, /*debug*/false));
    //function post to facebook fan page
    function postToFbFanPage() {
        var page_id = 'YOUR_PAGE_ID';
        FB.api('/me/accounts', function(response) {
            if (response.data[0].access_token) {
                FB.api('/' + page_id + '/feed',
                        'post',
                        {
                            message: "YOUR_DESCRIPTION",
                            picture: 'YOUR_PICTURE',
                            link: 'YOUR_LINK',
                            access_token: response.data[0].access_token
                        }
                , function(response) {
                    if (response.id) {
                        alert('Successful with id: ' + response.id);
                    }
                });
            }
        });
    }
</script>
<div id="fb-root"></div>
<a href="javascript:postToFbFanPage()">Post to Facebook Fanpage</a>
